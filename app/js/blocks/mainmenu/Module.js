'use strict';

var App = require('app');
var Controller = require('./controllers/Controller.js');

App.commands.setHandler('initMainMenu', function (options) {
	var controller = new Controller();
	controller.init();
});