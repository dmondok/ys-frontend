// Views > Views.Header
'use strict';

var $ = require('jquery');
var Marionette = require('marionette');
var App = require('app');
var template = require('../templates/template.hbs');
var Circle = require('progressCircle');
var IScroll = require('iscroll');

var HeaderView = Marionette.ItemView.extend({
	// className: 'main-menu',

	// events: {

	// },

	template : function (serializedModel) {
		return template(serializedModel);
	},

	initialize: function () {

	},

	onShow: function () {
		this.myscroll = new IScroll('#main-menu-region', {
			mouseWheel: true,
			scrollbars: true,
			disableMouse: true,
			interactiveScrollbars: true,
			bounce: false
		});
	// 	var progressBar = new Circle('#profilebar-progress', {
	// 		strokeWidth: 6,
	// 		color: "#fff",
	// 		trailColor: "rgba(255, 255, 255, 0.5)",
	// 	});
	// 	progressBar.animate(.73);
	// 	// var line = new ProgressBar.Line('#container');
	}
});

module.exports = HeaderView;