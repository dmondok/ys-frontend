// Controllers > Controllers.Navbar
'use strict';

var Backbone = require('backbone');
var Marionette = require('marionette');
var App = require('app');
var View = require('../views/ItemView.js');

var Controller = Marionette.Controller.extend({
	init: function (config) {
		var view = new View();
		App.getRegion('regionMainMenu').show(view);
	}
});

module.exports = Controller;