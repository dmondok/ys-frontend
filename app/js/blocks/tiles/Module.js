'use strict';

var App = require('app');
var Controller = require('./controllers/Controller.js');

App.commands.setHandler('initTiles', function (options) {
	var controller = new Controller();
	controller.init();
});