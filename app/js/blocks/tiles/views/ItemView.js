// Views > Views.Header
'use strict';

var $ = require('jquery');
var Marionette = require('marionette');
var App = require('app');
var template = require('../templates/template.hbs');
var Circle = require('progressCircle')

var HeaderView = Marionette.ItemView.extend({
	className: 'tiles',

	// events: {

	// },

	template : function (serializedModel) {
		return template(serializedModel);
	},

	initialize: function () {

	},

	onShow: function () {
	}
});

module.exports = HeaderView;