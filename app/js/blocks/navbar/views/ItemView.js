// Views > Views.Header
'use strict';

var $ = require('jquery');
var Marionette = require('marionette');
var App = require('app');
var template = require('../templates/nav.hbs');

var HeaderView = Marionette.ItemView.extend({
	// tagName: 'nav',
	// className: 'navbar',

	// events: {
	// 	'click .profile-toggle': 'toggleProfilebar',
	// 	'click .menu-toggle': 'toggleMainMenu'
	// },

	template : function (serializedModel) {
		return template(serializedModel);
	},

	initialize: function () {
		this.$body = $('body');
	},

	onShow: function () {
		var $body = $('body');
		$(window).on('resize', function(){
			$body.removeClass('show-profile');
		});
	}

	// toggleProfilebar: function (event) {
	// 	event.preventDefault();
	// 	var $body = this.$body;
	// 	var self = this;
	// 	$body.removeClass('show-main-menu').toggleClass('show-profile');
	// 	if ($body.hasClass('show-profile')) {
	// 		$body.append('<div id="profilebar-closer"></div>').children('#profilebar-closer').one('click', function (event) {
	// 			$(this).remove();
	// 			$body.removeClass('show-profile')
	// 		});
	// 	} else {
	// 		$('#profilebar-closer').remove();
	// 	}
	// },

	// toggleMainMenu: function (event) {
	// 	event.preventDefault();
	// 	$('body').removeClass('show-profile').toggleClass('show-main-menu');
	// }
});

module.exports = HeaderView;