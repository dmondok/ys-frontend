// Models > Models.Appconfig
'use strict';

var $ = require('jquery');
var Backbone = require('backbone');
var App = require('app');

var config = require('../appconfig.json');

var Model = Backbone.NestedModel.extend({
});

var API = {
	getAppconfigEntity: function () {
		var appconfig = new Model({

		});
		var defer = $.Deferred();
		defer.resolve(user);
		return defer.promise();
	}
}

// return promise when user has been requested
App.reqres.setHandler('entity:appconfig', function (options) {
	// return API.getAppconfigEntity();
	return config;
});

module.exports = Model;