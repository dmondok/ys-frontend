// Core > Initializer > Controllers.Initializer
'use strict';

var $ = require('jquery');
var _ = require('underscore');
var Backbone = require('backbone');
var Marionette = require('marionette');
var App = require('app');

// require('entities/Model.Appconfig');

var Controller = Marionette.Controller.extend({
	init: function () {
		var controller = this;

		_.bindAll(this, 'fetchApp', 'initApp');
		this.fetchApp();
	},

	fetchApp: function () {
		var fetchingAppconfig = App.reqres.request('entity:appconfig');
		$.when(fetchingAppconfig).done(this.initApp);
	},

	initApp: function (config) {
		App.commands.execute('initNavbar', config);
		App.commands.execute('initProfilebar', config);
		App.commands.execute('initMainMenu', config);
	}
});

module.exports = Controller;