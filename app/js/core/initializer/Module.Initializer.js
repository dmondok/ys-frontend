// Core > Module.Initializer
'use strict';

var App = require('app');
var Controller = require('./controllers/Controller.Initializer.js');
var controller = new Controller();

App.addInitializer(function () {
	controller.init();
});