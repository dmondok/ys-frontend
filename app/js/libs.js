var $ = require('jquery'),
	_ = require('underscore'),
	Backbone = require('backbone');

Backbone.$ = $;

/*--------------
	Backbone Framework & Extensions
---------------*/
require('marionette');
// require('backbone-localStorage');
require('backbone-nested-model');


/*--------------
	Bootstrap Framework & Extensions
---------------*/
// require('bootstrap');


/*--------------
	Velocity
---------------*/
require('velocity');

/*--------------
	IScroll
---------------*/
var IScroll = require('iscroll');
IScroll.prototype.refreshOnContentResize = function() {
  console.log('test');
  var _resize2 = this._resize.bind(this);
  var _resize = function (event) {
    console.log(event);
    _resize2();
  };

  // make sure the scroller can fire overflow events
  // this might be moved to user-css
  // this.scroller.style.height = '100%';
  // this.scroller.style.overflow = 'auto';

  // Internet Explorer
  this.wrapper.addEventListener('resize', _resize, true);
  // Firefox
  this.wrapper.addEventListener('overflowchanged', _resize, true);
  // Webkit, Blink
  this.wrapper.addEventListener('overflow', _resize, true);
  this.wrapper.addEventListener('underflow', _resize, true);

  this.on('destroy', function () {
    // Internet Explorer
    this.wrapper.removeEventListener('resize', _resize, true);
    // Firefox
    this.wrapper.removeEventListener('overflowchanged', _resize, true);
    // Webkit, Blink
    this.wrapper.removeEventListener('overflow', _resize, true);
    this.wrapper.removeEventListener('underflow', _resize, true);
  });
};

_.noConflict();
$.noConflict(true);
Backbone.noConflict();

