// MajorElement State Module

var $ = require('jquery')
	_ = require('underscore');

var Module = {
	initialize: function () {
		var self = this;

		_.bindAll(this, 'toggleElements');

		_.extend(this, {
			$body: $('body'),
			$menuToggles: $('.menu-toggle'),
			$profilebarToggles: $('.profile-toggle'),
			currentClass: null
		});

		this.$body.removeClass('show-main-menu show-profile');
		this.$menuToggles.on('click', function(event) {
			event.preventDefault();
			self.currentClass = self.toggleElements('show-main-menu');
		});

		this.$profilebarToggles.on('click', function(event) {
			event.preventDefault();
			self.currentClass = self.toggleElements('show-profile');
		});
	},

	toggleElements: function (className) {
		if (! this.currentClass || this.currentClass !== className) {
			this.$body.removeClass(this.currentClass).addClass(className);
			return className;
		} else if (this.currentClass === className) {
			this.$body.removeClass(className)
			return null;
		}
	}
};

module.exports = Module;