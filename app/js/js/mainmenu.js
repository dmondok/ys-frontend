// MajorElement State Module

var $ = require('jquery')
	_ = require('underscore'),
	IScroll = require('iscroll');

var Module = {
	initialize: function (config) {
		this.myscroll: new IScroll('#main-menu-region', {
			mouseWheel: true,
			scrollbars: true,
			disableMouse: true,
			interactiveScrollbars: true,
			bounce: false
		});
	}
};

module.exports = Module;