// MajorElement State Module

var $ = require('jquery')
	_ = require('underscore'),
	IScroll = require('iscroll'),
	Circle = require('progressCircle');

var Module = {
	initialize: function (config) {
		var self = this;

		var defaultConfig = {
			progress: {
				color: 'black',
				progress: .5
			}
		}

		config = _.extend(defaultConfig, config);

		// _.bindAll(this, 'toggleElements');

		_.extend(this, {
			$region: $('#profilebar-region'),
			$prgress: $('#profilebar-progress'),
			myscroll: new IScroll('#profilebar-region', {
				mouseWheel: true,
				scrollbars: true,
				disableMouse: true,
				interactiveScrollbars: true,
				bounce: false
			})
		});

		if (this.$prgress.length) {
			this.progressBar = new Circle('#profilebar-progress', {
				strokeWidth: 6,
				color: config.progress.color,
				trailColor: 'rgba(' + config.progress.color + ', 0.5)',
			});
			this.progressBar.animate(config.progress.progress);
		}
	}
};

module.exports = Module;