var Marionette = require('marionette');

var App = require('app');

var Controller = require('./controllers/Controller.js');

var Router = Marionette.AppRouter.extend({
	appRoutes: {
		'vote': 'showVote',
	}
});

var API = {
	showVote: function () {
		var controller = new Controller();
		controller.show();
	},
}

App.commands.setHandler('vote:show', function () {
	App.navigate('vote');
	API.showVote();
});

App.addInitializer(function () {
	new Router({
		controller: API
	});
});