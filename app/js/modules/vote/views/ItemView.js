'use strict';

var Marionette = require('marionette');
var App = require('app');
var template = require('../templates/template.hbs');

module.exports = Marionette.ItemView.extend({
	// tagName: '',
	// className: 'main-wrapper',

	// events: {},
	// modelEvents: {},

	// ui: {},
	// trigger: {},

	// initialize: function () {

	// },

	template : function(serializedModel) {
		return template(serializedModel);
	}
});