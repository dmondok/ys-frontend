'use strict';

var Marionette = require('marionette');
var App = require('app');
var template = require('../templates/template.hbs');
// var IScroll = require('iscroll');

module.exports = Marionette.ItemView.extend({
	// tagName: '',
	// className: 'main-wrapper',

	// events: {
	// 	'click a': 'triggerTile',
	// 	'click .share-button': 'share'
	// },
	// modelEvents: {},

	// ui: {},
	// trigger: {},

	// initialize: function () {

	// },

	template : function(serializedModel) {
		return template(serializedModel);
	},

	triggerTile: function (event) {
		event.preventDefault();
		event.stopImmediatePropagation();
		App.commands.execute('vote:show');
	},

	share: function (event) {
		event.preventDefault();
		event.stopImmediatePropagation();
		alert('Share!')
	},

	onShow: function () {

	}
});