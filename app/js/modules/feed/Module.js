var Marionette = require('marionette');

var App = require('app');

var Controller = require('./controllers/Controller.js');

var Router = Marionette.AppRouter.extend({
	appRoutes: {
		'': 'showFeed',
	}
});

var API = {
	showFeed: function () {
		var controller = new Controller();
		controller.show();
	},
}

App.commands.setHandler('feed:show', function () {
	App.navigate('');
	API.showFeed();
});

App.addInitializer(function () {
	new Router({
		controller: API
	});
});