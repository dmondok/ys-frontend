'use strict';

var Marionette = require('marionette');

var App = require('app');

var View = require('../views/ItemView.js');

module.exports = Marionette.Controller.extend({
	show: function (options) {
		this.region = App.getRegion('regionMain');
		this.view = new View();

		this.initEvents();

		this.region.show(this.view);
	},

	initEvents: function () {
		var controller = this,
			region = this.region;

		controller.listenTo(region, 'show', function () {

			// destroy controller if view changes
			controller.listenTo(region, 'empty', function () {
				controller.destroy();
			});
		});
	}
});