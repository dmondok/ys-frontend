'use strict';

var $ = require('jquery'),
	_ = require('underscore'),
	Backbone = require('backbone'),
	Marionette = require('marionette');

var App = new Marionette.Application();

App.addRegions({
	regionNavbar: '#navbar-region',
	regionProfilebar: '#profilebar-region',
	regionMain: '#main-region',
	regionMainMenu: '#main-menu-region'
});

App.navigate = function(route,  options) {
	options || (options = {});
	Backbone.history.navigate(route, options);
};

App.getCurrentRoute = function(){
	return Backbone.history.fragment;
};

App.addInitializer(function () {

	// console.log(myscroll.scroller);
});

App.on('start', function() {
	console.log('App started');
	if (Backbone.history) {
		if (!Backbone.history.start()) {
			console.log('404');
			App.commands.execute('show404');
		}
	}

	var elemToggleModule = require('./js/elemToggle.js');
	elemToggleModule.initialize();

	var profilebarModule = require('./js/profilebar.js');
	profilebarModule.initialize({
		progress: {
			color: 'white',
			progress: .73
		}
	});
});

window.ys_app = {
	start: function (options) {
		App.start(options);
	}
};

module.exports = App;