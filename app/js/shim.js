require('./core/initializer/Module.Initializer.js');

require('./modules/feed/Module.js');
require('./modules/vote/Module.js');

require('./blocks/navbar/Module.js');
require('./blocks/profilebar/Module.js');
require('./blocks/tiles/Module.js');
require('./blocks/mainmenu/Module.js');