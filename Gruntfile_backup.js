'use strict';

var _ = require('underscore'),
	packageJSON = require('./package.json'),
	libMods = _.map(packageJSON.browser, function(value, key, list){	return key;});
var shims = require('./shims'),
	sharedModules = Object.keys(shims).concat(libMods),
	remapify = require('remapify'),
	minifyify = require('minifyify');

module.exports = function (grunt) {

	// Load grunt tasks automatically
	require('load-grunt-tasks')(grunt);

	// Time how long tasks take. Can help when optimizing build times
	require('time-grunt')(grunt);

	// Configurable paths
	var config = {
		app: 'app',
		dist: 'dist'
	};

	// Define the configuration for all the tasks
	grunt.initConfig({

		// Project settings
		config: config,

		watch: {
			options: {
				// nospawn: true,
				livereload: true
			},
			js: {
				files: '.tmp/js/*.js'
			},
			gruntfile: {
				files: ['Gruntfile.js'],
				tasks: ['browserify']
			},
			sass: {
				files: ['<%= config.app %>/styles/**/{,*/}*.{scss,sass}'],
				tasks: ['sass:server', 'autoprefixer']
			},
			styles: {
				files: ['<%= config.app %>/styles/{,*/}*.css'],
				tasks: ['newer:copy:styles', 'autoprefixer']
			},
			livereload: {
				options: {
					livereload: '<%= connect.options.livereload %>'
				},
				files: [
					'<%= config.app %>/{,*/}*.html',
					'.tmp/styles/{,*/}*.css',
					'<%= config.app %>/images/{,*/}*'
				]
			}
		},

		// The actual grunt server settings
		connect: {
			options: {
				port: 9000,
				open: true,
				livereload: 35729,
				// Change this to '0.0.0.0' to access the server from outside
				hostname: 'localhost'
			},
			livereload: {
				options: {
					middleware: function(connect) {
						return [
							connect.static('.tmp'),
							// connect().use('/app/js/vendor', connect.static('./app/js/vendor')),
							connect.static(config.app)
						];
					}
				}
			},
			test: {
				options: {
					open: false,
					port: 9001,
					middleware: function(connect) {
						return [
							connect.static('.tmp'),
							connect.static('test'),
							// connect().use('/app/js/vendor', connect.static('./app/js/vendor')),
							connect.static(config.app)
						];
					}
				}
			},
			dist: {
				options: {
					base: '<%= config.dist %>',
					livereload: false
				}
			}
		},

		// Empties folders to start fresh
		clean: {
			dist: {
				files: [{
					dot: true,
					src: [
						'.tmp',
						'<%= config.dist %>/*',
						'!<%= config.dist %>/.git*'
					]
				}]
			},
			server: '.tmp'
		},

		browserify: {
			options: {
				debug: true,
				watch: true
				// keepAlive: true
			},
			lib: {
				options: {
					transform: ['browserify-shim'],
					require: sharedModules
				},
				files: {
					'.tmp/js/libs.js': ['app/js/libs.js']
				}
			},
			main: {
				options: {
					transform: ['hbsfy'],
					external: sharedModules,
					preBundleCB: function (b) {
						// grunt.log.write(__dirname);
						b.plugin(remapify, [{
								src: './**/*.json',
								expose: 'configs',
								cwd: './app/js/configs'
							}, {
								src: './**/*.js',
								expose: 'controllers',
								cwd: './app/js/controllers'
							}, {
								src: './**/*.js',
								expose: 'entities',
								cwd: './app/js/entities'
							}, {
								src: './**/*.js',
								expose: 'helpers',
								cwd: './app/js/helpers'
							}, {
								src: './**/*.js',
								expose: 'modules',
								cwd: './app/js/modules'
							}, {
								src: './**/*.js',
								expose: 'views',
								cwd: './app/js/views'
							}, {
								src: './**/*.html',
								expose: 'tpl',
								cwd: './app/templates'
							}, {
								src: './**/*.hbs',
								expose: 'hbs',
								cwd: './app/hbs'
							}
						]);
						b.plugin(minifyify, {map: '.tmp/js/main.js'});
					},
					alias: [
						'<%= config.app %>/js/app.js:app'
					]
				},
				files: {
					'.tmp/js/main.js': [
						'app/js/shim.js',
						'app/js/main.js'

						// '<%= config.app %>/js/modules/*.js',
						// '<%= config.app %>/js/apps/*/*.js'
					]
				}
			}
		},

		// Compiles Sass to CSS and generates necessary files if requested
		sass: {
			options: {
				loadPath: [
					'bower_components/bootstrap-sass-official/assets/stylesheets',
					'bower_components/fontawesome/scss',
					'bower_components/slick.js/slick',
					'bower_components/dropzone/downloads/css'
				]
			},
			dist: {
				files: [{
					expand: true,
					cwd: '<%= config.app %>/styles',
					src: ['*.scss'],
					dest: '.tmp/styles',
					ext: '.css'
				}]
			},
			server: {
				files: [{
					expand: true,
					cwd: '<%= config.app %>/styles',
					src: ['*.scss'],
					dest: '.tmp/styles',
					ext: '.css'
				}]
			}
		},

		// Add vendor prefixed styles
		autoprefixer: {
			options: {
				browsers: ['last 3 version', 'Explorer >= 9']
			},
			dist: {
				files: [{
					expand: true,
					cwd: '.tmp/styles/',
					src: '{,*/}*.css',
					dest: '.tmp/styles/'
				}]
			}
		},

		// Copies remaining files to places other tasks can use
		copy: {
			dist: {
				files: [{
					expand: true,
					dot: true,
					cwd: '<%= config.app %>',
					dest: '<%= config.dist %>',
					src: [
						'*.{ico,png,txt}',
						'.htaccess',
						'img/{,*/}*.webp',
						'{,*/}*.html',
						'styles/fonts/{,*/}*.*'
					]
				}]
			},
			fonts: {
				files: [{
					expand: true,
					flatten: true,
					filter: 'isFile',
					cwd: 'bower_components',
					src: ['**/fonts/*'],
					dest: '.tmp/fonts'
				}]
			},
			styles: {
				expand: true,
				dot: true,
				cwd: '<%= config.app %>/styles',
				dest: '.tmp/styles/',
				src: '{,*/}*.css'
			},
			modernizr: {
				expand: true,
				cwd: 'bower_components/modernizr/',
				src: 'modernizr.js',
				dest: '.tmp/vendor/'
			}
		},

		concurrent: {
			server: [
				'sass:server',
				'copy:styles',
				'copy:fonts',
				'copy:modernizr'
			],
			test: [
				'copy:styles'
			],
			dist: [
				'sass',
				'copy:styles',
				'imagemin',
				'svgmin'
			]
		},
	});



	grunt.registerTask('serve', function (target) {
		if (target === 'dist') {
			return grunt.task.run(['build', 'connect:dist:keepalive']);
		}

		grunt.task.run([
			'clean:server',
			'browserify',
			'concurrent:server',
			'autoprefixer',
			'connect:livereload',
			'watch'
		]);
	});

	grunt.registerTask('server', function (target) {
		grunt.log.warn('The `server` task has been deprecated. Use `grunt serve` to start a server.');
		grunt.task.run([target ? ('serve:' + target) : 'serve']);
	});

	grunt.registerTask('build', [
		'clean:dist',
		'useminPrepare',
		'concurrent:dist',
		'autoprefixer',
		'concat',
		'cssmin',
		'uglify',
		'copy:dist',
		'requirejs',
		'modernizr',
		'usemin',
		'htmlmin'
	]);

	grunt.registerTask('default', [
		'browserify'
	]);
};