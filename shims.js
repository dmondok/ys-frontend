module.exports = {
/*--------------
	jQuery
---------------*/
	jquery: {
		exports: "jQuery"
	},
/*--------------
	underscore
---------------*/
	underscore: {
		exports: "_"
	},
/*--------------
	Backbone Framework & Extensions
---------------*/
	backbone: {
		exports: "Backbone",
		depends: {
			underscore: "underscore",
			jquery: "jQuery"
		}
	},
	marionette: {
		exports: "Marionette",
		depends: {
			underscore: "underscore",
			jquery: "jQuery",
			backbone: "Backbone"
		}
	},
	"backbone-nested-model": {
		depends: {
			underscore: "underscore",
			jquery: "jQuery",
			backbone: "Backbone"
		}
	},
/*--------------
	Moment
---------------*/
	// moment: {
	// 	exports: "moment"
	// },
/*--------------
	Bootstrap Framework & Extensions
---------------*/
	// bootstrap: {
	// 	depends: {
	// 		jquery: "$"
	// 	}
	// },
	// "bootstrap-affix": {
	// 	depends: {
	// 		jquery: "$"
	// 	}
	// },
	// "bootstrap-alert": {
	// 	depends: {
	// 		jquery: "$"
	// 	}
	// },
	// "bootstrap-button": {
	// 	depends: {
	// 		jquery: "$"
	// 	}
	// },
	// "bootstrap-carousel": {
	// 	depends: {
	// 		jquery: "$"
	// 	}
	// },
	// "bootstrap-collapse": {
	// 	depends: {
	// 		jquery: "$"
	// 	}
	// },
	// "bootstrap-dropdown": {
	// 	depends: {
	// 		jquery: "$"
	// 	}
	// },
	// "bootstrap-modal": {
	// 	depends: {
	// 		jquery: "$"
	// 	}
	// },
	// "bootstrap-popover": {
	// 	depends: {
	// 		jquery: "$"
	// 	}
	// },
	// "bootstrap-scrollspy": {
	// 	depends: {
	// 		jquery: "$"
	// 	}
	// },
	// "bootstrap-tab": {
	// 	depends: {
	// 		jquery: "$"
	// 	}
	// },
	"bootstrap-tooltip": {
		depends: {
			jquery: "$"
		}
	},
	// "bootstrap-transition": {
	// 	depends: {
	// 		jquery: "$"
	// 	}
	// },
/*--------------
	Velocity
---------------*/
	velocity: {
		depends: {
			jquery: "jQuery"
		}
	},
/*--------------
	iScroll
---------------*/
	iscroll: {
		exports: "IScroll"
	}
/*--------------
	MISC
---------------*/
	// dropzone: {
	// 	exports: "Dropzone",
	// 	depends: {
	// 		jquery: "jQuery"
	// 	}
	// }
	// "backbone-localStorage": {
	// 	depends: {
	// 		underscore: "underscore",
	// 		jquery: "jQuery",
	// 		backbone: "Backbone"
	// 	}
	// },
	// "jquery-lazyload": {
	// 	depends: {
	// 		jquery: "jQuery"
	// 	}
	// },
	// 'loadImage': {
	// 	exports: 'loadImage',
	// 	depends: {
	// 		jquery: "$"
	// 	}
	// },
	// canvas2blob: {
	// 	depends: {
	// 		jquery: "jQuery"
	// 	}
	// },
	// slickjs: {
	// 	depends: {
	// 		jquery: "jQuery"
	// 	}
	// },
	// "ui-widget": {
	// 	depends: {
	// 		jquery: "jQuery"
	// 	}
	// },
	// "blueimp-file-upload": {
	// 	depends: {
	// 		jquery: "$"
	// 	}
	// },
	// "blueimp-file-upload-image": {
	// 	depends: {
	// 		jquery: "$",
	// 		loadImage: "loadImage"
	// 	}
	// },
	// "blueimp-file-upload-iframe": {
	// 	depends: {
	// 		jquery: "$"
	// 	}
	// },
	// "blueimp-file-upload-process": {
	// 	depends: {
	// 		jquery: "$"
	// 	}
	// }
};